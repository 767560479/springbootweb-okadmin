package com.example.springbootweb.dao;


import com.example.springbootweb.pojo.AuctionUser;
import com.example.springbootweb.pojo.AuctionUserExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Mapper
@Repository
public interface AuctionUserMapper {
    int countByExample(AuctionUserExample example);

    int deleteByExample(AuctionUserExample example);

    int deleteByPrimaryKey(Integer userid);

    int insert(AuctionUser record);

    int insertSelective(AuctionUser record);

    List<AuctionUser> selectByExample(AuctionUserExample example);

    AuctionUser selectByPrimaryKey(Integer userid);

    int updateByExampleSelective(@Param("record") AuctionUser record, @Param("example") AuctionUserExample example);

    int updateByExample(@Param("record") AuctionUser record, @Param("example") AuctionUserExample example);

    int updateByPrimaryKeySelective(AuctionUser record);

    int updateByPrimaryKey(AuctionUser record);
}