package com.example.springbootweb.controller;

import com.example.springbootweb.pojo.AuctionUser;
import com.example.springbootweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Description:
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @author: zhengfei.tan
 * @since: 2020/1/18 22:18
 * @history: 1.2020/1/18 created by zhengfei.tan
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping("listUser")
    public List<AuctionUser> allUserList(){
        return userService.allUserList();
    }

    @RequestMapping("index")
    public String index() {
        return "index";
    }
}
