package com.example.springbootweb.service;

import com.example.springbootweb.dao.AuctionUserMapper;
import com.example.springbootweb.pojo.AuctionUser;
import com.example.springbootweb.pojo.AuctionUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @author: zhengfei.tan
 * @since: 2020/1/18 22:16
 * @history: 1.2020/1/18 created by zhengfei.tan
 */
@Service
public class UserService {
    @Autowired
    private AuctionUserMapper auctionUserMapper;

    public List<AuctionUser> allUserList() {
        AuctionUserExample auctionUserExample = new AuctionUserExample();
        AuctionUserExample.Criteria criteria = auctionUserExample.createCriteria();
        return auctionUserMapper.selectByExample(auctionUserExample);
    }
}
